using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flickering :
MonoBehaviour
{
    // This is based on my own flashlight, the values and the behaviour is the same

    void Start() {}

    /*Modes :
     * Close = Range 10; Spotangle 120;
     * Normal = Range 34; Spotangle 52;
     * Far = Range 95 ; Spotangle 14;
     */

    public float lightmode = 0, lightrange = 0;

    // Update is called once per frame
    void Update()
    {

        //The Scroll will decide which mode (or value) the flashlight modulation will take :3
        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            //-------Coefficient based

           /* lightrange += Input.GetAxis("Mouse ScrollWheel");
            if (34 * lightrange > 119) { lightrange == 118; }
            if (34 * lightrange < 13) { lightrange == 13; }
            
            GetComponent<Light>().spotAngle = 52 / lightrange;
            GetComponent<Light>().range = 34 * lightrange;

            //Max Range 306 ; Spot angle 6*/

            //-------Coefficient based       Rework Required     


            //-------Modes
            lightmode += (int)(Input.GetAxis("Mouse ScrollWheel") * 10);
            if (lightmode <= 0) lightmode = 0;
            if (lightmode >= 5) lightmode = 4;


            switch (lightmode)
            {
                case 0:
                    GetComponent<Light>().enabled = false;
                    break;
                case 1:
                    GetComponent<Light>().enabled = true;
                    GetComponent<Light>().range = 10;
                    GetComponent<Light>().spotAngle = 120;
                    break;
                case 2:
                    GetComponent<Light>().enabled = true;
                    GetComponent<Light>().range = 34;
                    GetComponent<Light>().spotAngle = 52;
                    break;
                case 3:
                    GetComponent<Light>().enabled = true;
                    GetComponent<Light>().range = 95;
                    GetComponent<Light>().spotAngle = 14;
                    break;
                case 4:
                    GetComponent<Light>().enabled = false;
                    break;
            }
            //-------Modes - Next idea : SOS automated
        }
    }
}